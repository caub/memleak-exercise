const path = require('path');
const fs = require('fs');
const nameGen = require('./name');
const Handlebars = require('handlebars');
const http = require('http');

Handlebars.registerHelper('name', nameGen);

const templateStr = fs.readFileSync(path.join(__dirname, 'template')).toString();

// console.log(templateStr);

const template = Handlebars.compile(templateStr);


const server = http.createServer(function (req, res) {
  res.end(template({}));
});



server.listen(3000);
