const namesGenerator = require('docker-namesgenerator');

const names = new Map();

module.exports = () => namesGenerator(name => names.has(name));
